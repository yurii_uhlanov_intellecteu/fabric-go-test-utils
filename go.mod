module bitbucket.org/yurii_uhlanov_intellecteu/fabric-go-test-utils

go 1.13

require (
	bitbucket.org/yurii_uhlanov_intellecteu/fabric-go-mockstub-impl v0.1.2
	github.com/hyperledger/fabric v1.4.3
)
